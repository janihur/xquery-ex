XSLT Snippets
=============

Started as XQuery snippets but then I had no change to work with XQuery for years so now mainly XSLT snippets.

Run with [Saxon](https://www.saxonica.com/):

```
./saxon-xslt.sh <INPUT_XML_FILE> <XSLT> <OUTPUT_PARAMETER>
```

where `<OUTPUT_PARAMETER>` coud be e.g.

* `-Tout`

make-example
------------

Example how to use `make` to run and test transformations.
