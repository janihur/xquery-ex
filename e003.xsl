<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
exclude-result-prefixes="jh"
xmlns:jh="https://bitbucket.org/janihur/xquery-ex"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
	<xsl:output indent="yes" method="xml" encoding="utf-8"/>

	<xsl:template name="jh:attr">
		<attr key="{local-name()}"><xsl:value-of select="text()"/></attr>
	</xsl:template>
	
	<xsl:template match="text()"/>

	<xsl:template match="element()">
		<xsl:call-template name="jh:attr"/>
	</xsl:template>
	
	<xsl:template match="root">
		<attrlist>
			<!-- select all the nodes before the first c -->
			<xsl:apply-templates select="child::c[1]/preceding::*"/>
			
			<!-- select all the c nodes -->
			<xsl:for-each-group select="child::c" group-by="1">
				<attr key="clist">
					<xsl:for-each select="current-group()">
						<xsl:call-template name="jh:attr"/>
					</xsl:for-each>
				</attr>
			</xsl:for-each-group>

			<!-- select all the nodes after the last c -->
			<xsl:apply-templates select="child::c[last()]/following::*"/>
		</attrlist>
	</xsl:template>
	
</xsl:stylesheet>
