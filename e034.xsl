<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e034-input.xml?>
<!--
how to inject new attribute to the element
-->
<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:root="http://root.invalid"
	exclude-result-prefixes="root"
>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:strip-space elements="*"/>

	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="root:id">
		<!-- this will introduce (correct but unwanted) xmlns="" namespace declaration -->
		<!--
		<id xmlns:foo="http://foo.invalid" foo:nil="true" type="String">1234</id>
		-->
		<!-- this won't -->
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:attribute name="type">string</xsl:attribute>
			<xsl:text>1234</xsl:text>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
