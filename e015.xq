xquery version "1.0";

(: reduce a list of values to a single value :)

declare function local:reduce($input as element()) as xs:string {
	if (count($input/*[. = 'SUCCESS']) > 0)
	then
		'SUCCESS'
	else
		'FAIL'
};

declare variable $tests :=
	<tests>
		<test id="1" expected="FAIL"/>
		<test id="2" expected="SUCCESS">
			<item>SUCCESS</item>
		</test>
		<test id="3" expected="FAIL">
			<item>FAIL</item>
		</test>
		<test id="4" expected="SUCCESS">
			<item>SUCCESS</item>
			<item>FAIL</item>
		</test>
		<test id="5" expected="SUCCESS">
			<item>SUCCESS</item>
			<item>SUCCESS</item>
		</test>
		<test id="6" expected="FAIL">
			<item>FAIL</item>
			<item>FAIL</item>
		</test>
	</tests>
;

<results>{
	for $test in $tests/*
	return
		let $got := local:reduce($test)
		let $outcome := if ($test/@expected = $got) then 'OK' else 'ERROR'
		return <result id="{$test/@id}" outcome="{$outcome}">{$got}</result>
}</results>