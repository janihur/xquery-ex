#!/bin/bash

# ${1} source file for primary input
# ${2} main stylesheet file
# ${3} output option, e.g. -Tout

declare -r VERSION=9.9.1-8
declare -r SAXON=~/.m2/repository/net/sf/saxon/Saxon-HE/${VERSION}/Saxon-HE-${VERSION}.jar
java --class-path ${SAXON} net.sf.saxon.Transform -s:${1} -xsl:${2} ${3}
