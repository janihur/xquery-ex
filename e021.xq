xquery version "1.0";

(: splitting strings :)

declare function local:substring-after-last(
 $arg as xs:string?
,$delim as xs:string
) as xs:string? {
	if (contains($arg, $delim))
	then
		let $x := substring-after($arg, $delim)
		return local:substring-after-last($x, $delim)
	else
		$arg
};

local:substring-after-last('foo:bar1/bar2/bar3:car', ':'),
local:substring-after-last('foo|bar1/bar/bar3|car', ':'),
local:substring-after-last((), ':')
