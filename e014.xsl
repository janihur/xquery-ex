<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e014-input.xml?>
<!--
is time in a time range
-->
<xsl:stylesheet version="2.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="fn xs xsl"
>
	<xsl:output
		encoding="utf-8"
		indent="yes"
		method="xml"
		omit-xml-declaration="yes"
	/>

	<xsl:strip-space elements="*"/>

	<xsl:template match="text()"/>
	
	<xsl:template match="/input">
		<output>
			<xsl:apply-templates>
				<xsl:with-param name="rangeBegin" select="rangeBegin"/>
				<xsl:with-param name="rangeEnd" select="rangeEnd"/>
			</xsl:apply-templates>
		</output>
	</xsl:template>
	
	<xsl:template match="case">
		<xsl:param name="rangeBegin" as="xs:dateTime"/>
		<xsl:param name="rangeEnd" as="xs:dateTime"/>
		
		<xsl:variable name="now" as="xs:dateTime" select="current"/>
		
		<case id="{@id}" name="{@name}">
			<xsl:variable 
				name="inRange"
				select="($rangeBegin &lt;= $now) and ($now &lt;= $rangeEnd)"
			/>
			
			<xsl:choose>
				<xsl:when test="expected != $inRange">
					<result>FAILURE</result>
					<expected><xsl:value-of select="expected"/></expected>
					<got><xsl:value-of select="$inRange"/></got>
				</xsl:when>
				<xsl:otherwise>
					<result>OK</result>
				</xsl:otherwise>
			</xsl:choose>
		</case>
	</xsl:template>

</xsl:stylesheet>