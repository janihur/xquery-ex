<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e018-input-1.xml?>
<!--
Convert a plain string key-value list to a XML structure.
-->
<xsl:stylesheet version="2.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:jh="http://www.jani-hur.net"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	exclude-result-prefixes="fn xs"
>
	<xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	
	<xsl:template match="jh:x">
		<!-- XSLT 2.0 (the easy way) -->
		<jh:kvlist>
			<xsl:for-each select="fn:tokenize(., ':')">
				<!-- Doesn't work in a case where only key exists, e.g. :C: but that's fine for me now -->
				<jh:kv>
					<jh:k><xsl:sequence select="fn:substring-before(., ' ')"/></jh:k>
					<jh:v><xsl:sequence select="fn:substring-after(., ' ')"/></jh:v>
				</jh:kv>
			</xsl:for-each>
		</jh:kvlist>
		
		<!-- XSLT 1.0 (the hard way) -->
		<jh:kvlist>
			<xsl:call-template name="string-split">
				<xsl:with-param name="str" select="./text()"/>
			</xsl:call-template>
		</jh:kvlist>
	</xsl:template>
	
	<xsl:template name="string-split">
		<xsl:param name="str" select="."/>
		<xsl:if test="string-length($str) > 0">
			<!-- Doesn't work in a case where only key exists, e.g. :C: but that's fine for me now -->
			<xsl:variable name="kv">
				<xsl:choose>
					<xsl:when test="fn:string-length(fn:substring-before($str, ':')) > 0">
						<xsl:sequence select="fn:substring-before($str, ':')"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:sequence select="$str"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<jh:kv>
				<jh:k><xsl:sequence select="fn:substring-before($kv, ' ')"/></jh:k>
				<jh:v><xsl:sequence select="fn:substring-after($kv, ' ')"/></jh:v>
			</jh:kv>
			<xsl:call-template name="string-split">
				<xsl:with-param name="str" select="substring-after($str, ':')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
