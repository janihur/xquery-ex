<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e031-input.xml?>
<!--
Checking if element has 'value'.
-->
<xsl:stylesheet version="3.0"
	xmlns:array="http://www.w3.org/2005/xpath-functions/array"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:local="http://example.com"
	xmlns:map="http://www.w3.org/2005/xpath-functions/map"
	xmlns:math="http://www.w3.org/2005/xpath-functions/math"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="array err fn local map math xhtml xs"
>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="input">
		<output>
			<xsl:apply-templates/>
		</output>
	</xsl:template>

	<xsl:template match="a">
		<a>
			<original_value>
				<xsl:value-of select="."/>
			</original_value>
			<hasValue>
				<xsl:sequence select="local:hasValue(.)"/>
			</hasValue>
			<hasValue2>
				<xsl:sequence select="local:hasValue2(.)"/>
			</hasValue2>
			<empty>
				<xsl:sequence select="fn:empty(text())"/>
			</empty>
			<notEmpty>
				<xsl:sequence select="fn:not(fn:empty(text()))"/>
			</notEmpty>
		</a>
	</xsl:template>

	<xsl:function name="local:hasValue" as="xs:boolean">
		<xsl:param name="value" as="xs:string?"/>
		<xsl:sequence select="$value != ''"/>
	</xsl:function> 

	<xsl:function name="local:hasValue2" as="xs:boolean">
		<xsl:param name="value" as="xs:string?"/>
		<xsl:sequence select="fn:empty($value)"/>
	</xsl:function> 

	<!-- https://en.wikipedia.org/wiki/Identity_transform#XSLT_3.0 -->
	<!--<xsl:mode on-no-match="shallow-copy"/>-->
</xsl:stylesheet>
