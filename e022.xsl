<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e022-input.xml?>
<!--
Split a message with a list into multiple messages.
-->
<xsl:stylesheet version="2.0"
	xmlns:jh="http://www.jani-hur.net"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="jh xs fn"
>
	<xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="text()"/>

	<xsl:template match="/*">
		<messageList>
			<xsl:apply-templates select="jh:list">
				<xsl:with-param name="elemName" select="fn:local-name()"/>
				<xsl:with-param name="elemNs" select="fn:namespace-uri()"/>
			</xsl:apply-templates>
		</messageList>
	</xsl:template>
	
	<xsl:template match="jh:list">
		<xsl:param name="elemName"/>
		<xsl:param name="elemNs"/>

		<xsl:for-each select="jh:lorem">
			<xsl:element name="{$elemName}" namespace="{$elemNs}">
				<xsl:copy-of select="../preceding-sibling::*"/>
				<list xmlns="http://www.jani-hur.net">
					<xsl:copy-of select="."/>
				</list>
				<xsl:copy-of select="../following-sibling::*"/>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
