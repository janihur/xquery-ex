<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e004-input.xml?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<!--
A flat list to a nested hierachy based on IDs.
-->
	<xsl:output
		encoding="utf-8"
		indent="yes"
		method="xml"
		omit-xml-declaration="yes"
	/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="Subscriber">
		<assets>
			<xsl:apply-templates select="SubscriberAsset[not(ParentProductId = ../SubscriberAsset/ProductId)]"/>
		</assets>
	</xsl:template>

	<xsl:template match="SubscriberAsset">
		<xsl:variable name="id" select="ProductId"/>
		<xsl:variable name="subAssets" select="../SubscriberAsset[ParentProductId = $id]"/>
		<asset>
			<id><xsl:value-of select="$id"/></id>
			<name><xsl:value-of select="ProductName"/></name>
			<pid><xsl:value-of select="ParentProductId"/></pid>
			<xsl:if test="$subAssets">
				<subAssets>
					<xsl:apply-templates select="$subAssets"/>
				</subAssets>
			</xsl:if>
		</asset>
	</xsl:template>
	
</xsl:stylesheet>
