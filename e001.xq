xquery version "1.0";

(: "except" by value :)

declare namespace tns="http://jani-hur.net/xquery-ex";

declare variable $validItems as element() := <list>
  <item>A</item>
  <item>C</item>
  <item>D</item>
</list>
;

(:
declare variable $userInput as element() := <tns:AttributeList>
    <tns:Id attribute="email">john.doe@example.com</tns:Id>
    <tns:Attribute name="A">
      <tns:Value>1</tns:Value>
    </tns:Attribute>
    <tns:Attribute name="B">
      <tns:Value>2</tns:Value>
    </tns:Attribute>
    <tns:Attribute name="C">
      <tns:Value>3</tns:Value>
    </tns:Attribute>
  </tns:AttributeList>
;

let $suspiciousList := 
	<list>
	{
		for $attr in $userInput/tns:Attribute/@name
		return <item>{data($attr)}</item>
	}
	</list>

let $invalidValues := $suspiciousList/item[not(. = $validItems/item)]

return $invalidValues
:)

declare function local:has-invalid-values($userInput as element()) as xs:boolean {
	let $userList := 
		<list>
		{
			for $attr in $userInput/tns:Attribute/@name
			return <item>{data($attr)}</item>
		}
		</list>
	
	let $invalidItems := $userList/item[not(. = $validItems/item)]
	
	return count($invalidItems) > 0
};

local:has-invalid-values(
	<tns:AttributeList>
		<tns:Id attribute="email">john.doe@example.com</tns:Id>
		<tns:Attribute name="A">
		  <tns:Value>1</tns:Value>
		</tns:Attribute>
		<tns:Attribute name="C">
		  <tns:Value>3</tns:Value>
		</tns:Attribute>
	</tns:AttributeList>
),
local:has-invalid-values(
	<tns:AttributeList>
		<tns:Id attribute="email">john.doe@example.com</tns:Id>
		<tns:Attribute name="A">
		  <tns:Value>1</tns:Value>
		</tns:Attribute>
		<tns:Attribute name="B">
		  <tns:Value>2</tns:Value>
		</tns:Attribute>
		<tns:Attribute name="C">
		  <tns:Value>3</tns:Value>
		</tns:Attribute>
		<tns:Attribute name="D">
		  <tns:Value>3</tns:Value>
		</tns:Attribute>
		<tns:Attribute name="E">
		  <tns:Value>3</tns:Value>
		</tns:Attribute>
		<tns:Attribute name="F">
		  <tns:Value>3</tns:Value>
		</tns:Attribute>
	</tns:AttributeList>
)
