<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e028-input.xml?>
<!--
Process only elements that can be casted to a number.
-->
<xsl:stylesheet version="3.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="fn xs"
>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="input">
		<output>
			<xsl:variable name="numbers" select="a[. castable as xs:double]"/>
			<numbers>
				<xsl:for-each select="$numbers">
					<number><xsl:value-of select="fn:format-number(fn:number(.), '0.000')"/></number>
				</xsl:for-each>
			</numbers>
			<sum>
				<xsl:value-of select="fn:format-number(fn:sum(a[. castable as xs:double]), '0.000')"/>
			</sum>
		</output>
	</xsl:template>
</xsl:stylesheet>
