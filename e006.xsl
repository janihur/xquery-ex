<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e006-input.xml?>
<!--
convert arbitrary date string to xs:date.
-->
<xsl:stylesheet version="2.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:jh="http://www.jani-hur.net/xslt-functions"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="fn jh xs xsl"
>
	<xsl:output
		encoding="utf-8"
		indent="yes"
		method="xml"
		omit-xml-declaration="yes"
	/>

	<xsl:strip-space elements="*"/>

	<xsl:template match="text()"/>

	<xsl:variable name="months">
		<month shortName="Jan" longName="January"/>
		<month shortName="Feb" longName="February"/>
		<month shortName="Mar" longName="March"/>
		<month shortName="Apr" longName="April"/>
		<month shortName="May" longName="May"/>
		<month shortName="Jun" longName="June"/>
		<month shortName="Jul" longName="July"/>
		<month shortName="Aug" longName="August"/>
		<month shortName="Sep" longName="September"/>
		<month shortName="Oct" longName="October"/>
		<month shortName="Nov" longName="November"/>
		<month shortName="Dec" longName="December"/>
	</xsl:variable>

	<!--
	The picture strings are the same than in fn:format-date():
	https://www.w3.org/TR/xslt20/#function-format-date
	-->
	
	<xsl:function name="jh:monthOrdinal" as="xs:integer?">
		<xsl:param name="value" as="xs:string"/>
		<xsl:param name="picture" as="xs:string"/>
		<xsl:choose>
			<xsl:when test="$picture = '[MNn,3-3]'">
				<xsl:sequence select="fn:index-of($months/month/@shortName, $value)"/>
			</xsl:when>
			<xsl:when test="$picture = '[MNn]'">
				<xsl:sequence select="fn:index-of($months/month/@longName, $value)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:sequence select="()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	<xsl:function name="jh:ordinalMonth" as="xs:string?">
		<xsl:param name="ordinal" as="xs:integer"/>
		<xsl:param name="picture" as="xs:string"/>
		<xsl:choose>
			<xsl:when test="$picture = '[MNn,3-3]'">
				<xsl:sequence select="$months/month[$ordinal]/@shortName"/>
			</xsl:when>
			<xsl:when test="$picture = '[MNn]'">
				<xsl:sequence select="$months/month[$ordinal]/@longName"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:sequence select="()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	<xsl:function name="jh:date" as="xs:date?">
		<xsl:param name="value" as="xs:string"/>
		<xsl:param name="picture" as="xs:string"/>
		<xsl:choose>
			<xsl:when test="$picture = '[D01]-[MNn,3-3]-[Y0001]'">
				<xsl:variable name="dayStr" as="xs:string" select="fn:substring($value, 1, 2)"/>
				<xsl:variable name="monthStr" as="xs:string" select="fn:substring($value, 4, 3)"/>
				<xsl:variable name="yearStr" as="xs:string" select="fn:substring($value, 8, 4)"/>
				<xsl:variable name="monthOrdinal" as="xs:integer" 
					select="jh:monthOrdinal($monthStr, '[MNn,3-3]')"/>
				<xsl:variable name="isoDateStr" as="xs:string"
					select="fn:string-join(($yearStr, fn:format-number($monthOrdinal, '00'), $dayStr), '-')"/>
				<xsl:sequence select="xs:date($isoDateStr)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:sequence select="()"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	
	<xsl:template match="/input">
		<output>
			<xsl:apply-templates/>
		</output>
	</xsl:template>

	<xsl:template match="date">
		<xsl:variable name="dayStr" as="xs:string" select="fn:substring(., 1, 2)"/>
		<xsl:variable name="monthStr" as="xs:string" select="fn:substring(., 4, 3)"/>
		<xsl:variable name="yearStr" as="xs:string" select="fn:substring(., 8, 4)"/>
		<xsl:variable name="monthOrdinal" as="xs:integer"
			select="jh:monthOrdinal($monthStr, '[MNn,3-3]')"/>
		<xsl:variable name="isoDateStr" as="xs:string"
			select="fn:string-join(($yearStr, fn:format-number($monthOrdinal, '00'), $dayStr), '-')"/>

		<output>
			<input><xsl:value-of select="."/></input>
			<dayStr><xsl:value-of select="$dayStr"/></dayStr>
			<monthStr><xsl:value-of select="$monthStr"/></monthStr>
			<yearStr><xsl:value-of select="$yearStr"/></yearStr>
			<monthOrdinal><xsl:value-of select="$monthOrdinal"/></monthOrdinal>
			<monthLongname>
				<xsl:value-of select="jh:ordinalMonth($monthOrdinal, '[MNn]')"/>
			</monthLongname>
			<monthShortname>
				<xsl:value-of select="jh:ordinalMonth($monthOrdinal, '[MNn,3-3]')"/>
			</monthShortname>
			<isoDateStr><xsl:value-of select="$isoDateStr"/></isoDateStr>
			<xsDate><xsl:value-of select="xs:date($isoDateStr)"/></xsDate>
			<jhDate><xsl:value-of select="jh:date(., '[D01]-[MNn,3-3]-[Y0001]')"/></jhDate>
		</output>
	</xsl:template>

</xsl:stylesheet>
