<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e016-input.xml?>
<!--
remove optional postfix
-->
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
	<xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="text()"/>

	<xsl:template match="inputs">
		<outputs>
			<xsl:apply-templates select="input"/>
		</outputs>
	</xsl:template>
	
	<xsl:template match="input">
		<xsl:variable name="washed" select="substring-before(., '-SNAPSHOT')"/>
		<output>
			<xsl:choose>
				<xsl:when test="$washed">
					<xsl:value-of select="$washed"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="."/>
				</xsl:otherwise>
			</xsl:choose>
		</output>
	</xsl:template>
</xsl:stylesheet>
