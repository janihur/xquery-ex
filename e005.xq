xquery version "1.0";

(: Select element "dynamically". :)

declare variable $d as element() := <root>
	<foo>FOO VALUE</foo>
	<bar><em>BAR</em> VALUE</bar>
</root>
;

declare function local:f($x as xs:string) as xs:string* {
	(: returns 'BAR VALUE' :)
	string($d/*[name() = $x])
	(: returns ' VALUE' :)
	(: $d/*[name() = $x]/text() :)
};

local:f('bar')