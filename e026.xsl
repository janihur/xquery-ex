<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e026-input-1.xml?>
<!--
XSLT30: convert json to xml representation of json
-->
<xsl:stylesheet version="3.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="fn"
>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>

	<!-- the json payload is wrapped inside xml element. the name of the element is irrelevant -->
	<xsl:template match="*">
		<xsl:copy-of select="fn:json-to-xml(.)"/>
	</xsl:template>

</xsl:stylesheet>
