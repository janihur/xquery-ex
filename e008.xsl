<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e008-input.xml?>
<!--
calculate time difference (duration) between two xs:dateTime elements
-->
<xsl:stylesheet version="2.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:jh="http://www.jani-hur.net/xslt-functions"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="fn jh xs xsl"
>
	<xsl:output
		encoding="utf-8"
		indent="yes"
		method="xml"
		omit-xml-declaration="yes"
	/>

	<xsl:strip-space elements="*"/>

	<xsl:template match="text()"/>
	
	<xsl:template match="/input">
		<output>
			<xsl:apply-templates/>
		</output>
	</xsl:template>
	
	<xsl:template match="case">
		<case id="{@id}">
			<xsl:variable name="date1" as="xs:dateTime" select="(date1, current-dateTime())[1]"/>
			<xsl:variable name="date2" as="xs:dateTime" select="(date2, current-dateTime())[1]"/>
			<xsl:variable name="duration" as="xs:duration" select="$date1 - $date2"/>
			
			<xsl:copy-of select="date1|date2"/>

			<duration>
				<xsl:value-of select="$duration"/>
			</duration>
			<duration-in-seconds>
				<xsl:value-of select="$duration div xs:dayTimeDuration('PT1S')"/>
			</duration-in-seconds>
			<duration-in-minutes>
				<xsl:value-of select="ceiling($duration div xs:dayTimeDuration('PT1M'))"/>
			</duration-in-minutes>

		</case>
	</xsl:template>

</xsl:stylesheet>
