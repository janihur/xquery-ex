xquery version "1.0";

(: split sequence of strings (a key-value pairs) :)

declare function local:f($values as xs:string*) as element()? {
	if (not(empty($values)))
	then
		<kvlist>
		{
			let $separator :=  ': '
			for $kv in $values
			return
				<kv>
					<key>{substring-before($kv, $separator)}</key>
					<value>{substring-after($kv, $separator)}</value>
				</kv>
		}
		</kvlist>
	else ()
};

local:f(
	('foo: 3', 'bar: 3')
),
local:f(
	('foo: 2')
),
local:f(
	'foo: 1'
),
local:f(
	('foo: ')
),
local:f(
	('') (: this is not an empty sequence ! :)
),
local:f(
	()
)