xquery version "1.0";

(: calculate a single value from multiple input values :)

(: code under test :)
declare function local:f($a as xs:string, $b as xs:string*) as xs:string {
	if ($a = 'BAR' or $a = 'BATMAN')
	then
		'COMMON VALUE'
	else if ($a = 'FOO' and $b = 'BLUE')
	then
		'SPECIAL BLUE FOO VALUE'
	else
		'DEFAULT VALUE'
};

(: test definitions :)
declare variable $tests :=
	<tests>
		<test id="1" expected="DEFAULT VALUE">
			<a>FOO</a>
			<b>GREEN</b>
		</test>
		<test id="2" expected="SPECIAL BLUE FOO VALUE">
			<a>FOO</a>
			<b>BLUE</b>
		</test>
		<test id="3" expected="DEFAULT VALUE">
			<a>FOO</a>
			<b></b>
		</test>
		<test id="4" expected="COMMON VALUE">
			<a>BAR</a>
			<b></b>
		</test>
		<test id="5" expected="DEFAULT VALUE">
			<a>ZOO</a>
			<b></b>
		</test>
		<test id="6" expected="DEFAULT VALUE">
			<a>ZOO</a>
		</test>
		<test id="7" expected="COMMON VALUE">
			<a>BATMAN</a>
		</test>
	</tests>
;

(: test runner - prints only failures :)
<failures>{
	for $test in $tests/*
	let $got := local:f($test/a, $test/b)
	let $fail :=  $test/@expected != $got
	order by $test/@id ascending (: not needed but here as an example :)
	return 
		if ($fail)
		then
			<failure testid="{$test/@id}" >
				<expected>{data($test/@expected)}</expected>
				<got>{$got}</got>
			</failure>
		else
			()
}</failures>