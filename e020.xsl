<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e020-input-1.xml?>
<!--
Convert element name capitalization only.
-->
<xsl:stylesheet version="2.0"
	xmlns:bar="http://bar"
	xmlns:foo="http://foo"
	xmlns:jh="http://www.jani-hur.net"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
>
	<xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<!-- root element processed differently -->
	<xsl:template match="foo:Status">
		<xsl:element name="bar:status">
			<xsl:apply-templates select="@* | node()"/>
		</xsl:element>
	</xsl:template>

	<!-- convert element names: ElementNameAbc -> elementNameAbc and drop the namespace -->
	<xsl:template match="*">
		<xsl:variable name="oldName" select="fn:local-name()"/>
		<xsl:variable name="newName" select="fn:concat(fn:lower-case(fn:substring($oldName, 1, 1)),
													   fn:substring($oldName, 2))"/>
		<xsl:element name="{$newName}">
			<xsl:apply-templates select="@* | node()"/>
		</xsl:element>
	</xsl:template>

	<!-- copy as such -->
	<xsl:template match="@* | text() | comment() |processing-instruction()">
		<xsl:copy/>
	</xsl:template>

</xsl:stylesheet>
