<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e030-input.xml?>
<!--
Saxon specific regex pattern to split a string before capital letters
-->
<xsl:stylesheet version="3.0"
  xmlns:fn="http://www.w3.org/2005/xpath-functions"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="fn"
>
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="input">
    <output>
      <xsl:apply-templates/>
    </output>
  </xsl:template>

  <xsl:template match="a">
    <!-- saxon specific flag ;j enables Java regexes -->
    <!-- positive lookahead: (?=) -->
    <xsl:variable name="t1" select="fn:tokenize(., '(?=[A-Z])', ';j')"/>
    <xsl:variable name="t2" select="fn:string-join($t1, '_')"/>
    <xsl:variable name="t3" select="fn:upper-case($t2)"/>
    
    <a>
      <xsl:for-each select="$t1">
        <t1><xsl:value-of select="."/></t1>
      </xsl:for-each>
      <t2><xsl:value-of select="$t2"/></t2>
      <t3><xsl:value-of select="$t3"/></t3>
    </a>
  </xsl:template>
</xsl:stylesheet>
