<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e029-input.xml?>
<!--
Process only elements that can be casted to a dateTime.
-->
<xsl:stylesheet version="3.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="fn xs"
>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="input">
		<output>
			<dates id="1">
				<xsl:variable name="validDates" select="a[. castable as xs:dateTime]"/>
				<xsl:for-each select="$validDates">
					<date>
						<xsl:value-of select="."/>
					</date>
				</xsl:for-each>
			</dates>
			
			<dates id="2">
				<xsl:apply-templates select="a[. castable as xs:dateTime and xs:dateTime(.) > fn:current-dateTime()]"/>
			</dates>
		</output>
	</xsl:template>
	
	<xsl:template match="a">
		<date>
			<xsl:value-of select="."/>
		</date>
	</xsl:template>
</xsl:stylesheet>
