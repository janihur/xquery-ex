<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e023-input.xml?>
<!--
string-replace when replace xpath function is not available
-->
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="xs fn"
>
	<xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="/input">
		<output>
			<xsl:apply-templates select="number"/>
		</output>
	</xsl:template>

	<xsl:template match="number">
		<number>
			<xsl:call-template name="string-replace">
				<xsl:with-param name="text">
					<xsl:call-template name="string-replace">
						<xsl:with-param name="text">
							<xsl:call-template name="string-replace">
								<xsl:with-param name="text">
									<xsl:call-template name="string-replace">
										<xsl:with-param name="text">
											<xsl:call-template name="string-replace">
												<xsl:with-param name="text">
													<xsl:call-template name="string-replace">
														<xsl:with-param name="text">
															<xsl:value-of select="."/>
														</xsl:with-param>
														<xsl:with-param name="replace" select="'F'"/>
														<xsl:with-param name="by" select="'#15'"/>
													</xsl:call-template>
												</xsl:with-param>
												<xsl:with-param name="replace" select="'E'"/>
												<xsl:with-param name="by" select="'#14'"/>
											</xsl:call-template>
										</xsl:with-param>
										<xsl:with-param name="replace" select="'D'"/>
										<xsl:with-param name="by" select="'#13'"/>
									</xsl:call-template>
								</xsl:with-param>
								<xsl:with-param name="replace" select="'C'"/>
								<xsl:with-param name="by" select="'#12'"/>
							</xsl:call-template>
						</xsl:with-param>
						<xsl:with-param name="replace" select="'B'"/>
						<xsl:with-param name="by" select="'#11'"/>
					</xsl:call-template>
				</xsl:with-param>
				<xsl:with-param name="replace" select="'A'"/>
				<xsl:with-param name="by" select="'#10'"/>
			</xsl:call-template>
		</number>
	</xsl:template>

	<!-- https://stackoverflow.com/a/14597353/272735 -->
	<xsl:template name="string-replace">
		<xsl:param name="text"/>
		<xsl:param name="replace"/>
		<xsl:param name="by"/>
		<xsl:choose>
			<xsl:when test="contains($text, $replace)">
				<xsl:value-of select="substring-before($text, $replace)"/>
				<xsl:value-of select="$by"/>
				<xsl:call-template name="string-replace">
					<xsl:with-param name="text" select="substring-after($text, $replace)"/>
					<xsl:with-param name="replace" select="$replace"/>
					<xsl:with-param name="by" select="$by"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
</xsl:stylesheet>
