<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e017-input-2.xml?>
<!--
remove namespace from some elements
-->
<xsl:stylesheet version="2.0"
	xmlns:jh="http://www.jani-hur.net"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
>
	<xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<!-- these nodes will keep the namespace -->
	<xsl:template match="jh:root|jh:c1">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>

	<!-- these nodes will lost the namespace -->
	<xsl:template match="*">
		<xsl:element name="{fn:local-name()}">
			<xsl:apply-templates select="@* | node()"/>
		</xsl:element>
	</xsl:template>
	
	<!-- these nodes are copied as such -->
	<xsl:template match="@* | text() | comment() | processing-instruction()">
		<xsl:copy/>
	</xsl:template>
	
</xsl:stylesheet>
