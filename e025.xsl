<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e025-input.xml?>
<!--
XSLT3 functions, current time, timestring presentation formatting and local time to utc conversion.
Note XMLSpy (PE 2018 sp1) and Saxon (HE 9.9.1-6) has different default time picture string:

XMLSpy second granularity:    2021-05-20T08:51:18+03:00
Saxon sub-second granularity: 2021-05-20T08:51:18.215085+03:00
-->
<xsl:stylesheet version="3.0"
	xmlns:jh="http://www.jani-hur.net/xquery-ex"
	xmlns:array="http://www.w3.org/2005/xpath-functions/array"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:map="http://www.w3.org/2005/xpath-functions/map"
	xmlns:math="http://www.w3.org/2005/xpath-functions/math"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="array err fn map math xhtml xs"
>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/" name="xsl:initial-template">
		<xsl:variable name="ISO8601_PICTURE_1" select="'[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01][Z]'"/>
		<xsl:variable name="local-now" select="fn:current-dateTime()"/>
		<xsl:variable name="utc-now" select="fn:adjust-dateTime-to-timezone($local-now, xs:dayTimeDuration('PT0H'))"/>
		<output>
			<!--<_><xsl:value-of select="Q{http://www.altova.com/xslt-extensions}current-dateTime-no-TZ()"/></_>-->
			<_ id="1"><xsl:value-of select="fn:current-dateTime()"/></_>
			<_ id="2"><xsl:value-of select="$local-now"/></_>
			<_ id="3"><xsl:value-of select="fn:adjust-dateTime-to-timezone($local-now)"/></_>
			<_ id="4"><xsl:value-of select="fn:adjust-dateTime-to-timezone($local-now, xs:dayTimeDuration('PT0H'))"/></_>
			<_ id="5"><xsl:value-of select="fn:format-dateTime($local-now, $ISO8601_PICTURE_1)"/></_>
			<_ id="6"><xsl:value-of select="fn:format-dateTime(fn:adjust-dateTime-to-timezone($local-now, xs:dayTimeDuration('PT0H')), $ISO8601_PICTURE_1)"/></_>
			<_ id="7"><xsl:value-of select="$utc-now"/></_>
			<_ id="8"><xsl:value-of select="fn:format-dateTime($utc-now, '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]Z')"/></_>
			<_ id="9"><xsl:value-of select="jh:utc-dateTime()"/></_>
			<_ id="10"><xsl:value-of select="jh:utc-dateTime-str($local-now)"/></_>
		</output>
	</xsl:template>
	
	<xsl:function name="jh:utc-dateTime-str" as="xs:string">
		<xsl:param name="time" as="xs:dateTimeStamp"/>
		<xsl:sequence select="fn:format-dateTime(fn:adjust-dateTime-to-timezone($time, xs:dayTimeDuration('PT0H')), '[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]Z')"/>
	</xsl:function>
	
	<xsl:function name="jh:utc-dateTime" as="xs:dateTime">
		<xsl:sequence select="fn:adjust-dateTime-to-timezone(fn:current-dateTime(), xs:dayTimeDuration('PT0H'))"/>
	</xsl:function>
</xsl:stylesheet>
