<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e032-input.json.xml?>
<xsl:stylesheet version="3.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="fn xs"
>
	<xsl:output method="text"/>
	
	<xsl:template match="/json" name="xsl:initial-template">
		<!-- (1/3) transform JSON to XML representation of JSON -->
		<xsl:variable name="json-as-xml" select="fn:json-to-xml(.)"/>
		<!-- (2/3) manipulate the XML -->
		<xsl:variable name="transformed-xml">
			<xsl:apply-templates select="$json-as-xml" mode="root"/>
		</xsl:variable>
		<!-- (3/3) transform XML representation of JSON to JSON-->
		<xsl:value-of select="fn:xml-to-json($transformed-xml)"/>
	</xsl:template>
	
	<xsl:template match="fn:map" mode="root">
		<fn:map>
			<fn:array key="customers">
				<xsl:apply-templates select="fn:array[@key = 'contacts']/fn:map" mode="contact">
					<xsl:sort select="fn:string[@key = 'lastName']"/>
				</xsl:apply-templates>
			</fn:array>
		</fn:map>
	</xsl:template>
	
	<xsl:template match="fn:map" mode="contact">
		<fn:map>
			<fn:number key="id">
				<xsl:value-of select="fn:position()"/>
			</fn:number>
			<fn:map key="name">
				<fn:string key="first">
					<xsl:value-of select="fn:string[@key = 'firstName']"/>
				</fn:string>
				<fn:string key="last">
					<xsl:value-of select="fn:string[@key = 'lastName']"/>
				</fn:string>
			</fn:map>
		</fn:map>
	</xsl:template>
	
</xsl:stylesheet>
