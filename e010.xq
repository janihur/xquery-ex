xquery version "1.0";

(: filter duplicate values :)

let $x := 
<list>
{
	for $i in (0, 1, 1, 2, 0, 2, 2, (3 to 9), 3, 4)
	return <a>{$i}</a>
}
</list>
return 
	<list>
		{$x/a[not(.=preceding::a)]}
	</list>