<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e025-input.xml?>
<!--
Template XSLT 3.0 template with identity transform.
-->
<xsl:stylesheet version="3.0"
	xmlns:array="http://www.w3.org/2005/xpath-functions/array"
	xmlns:err="http://www.w3.org/2005/xqt-errors"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:map="http://www.w3.org/2005/xpath-functions/map"
	xmlns:math="http://www.w3.org/2005/xpath-functions/math"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="array err fn map math xhtml xs"
>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<!-- https://en.wikipedia.org/wiki/Identity_transform#XSLT_3.0 -->
	<xsl:mode on-no-match="shallow-copy"/>
</xsl:stylesheet>
