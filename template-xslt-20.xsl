<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e017-input-1.xml?>
<!--
XSLT 2.0 template with identity transform
-->
<xsl:stylesheet version="2.0"
	xmlns:jh="http://www.jani-hur.net"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
>
	<xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<!-- identity transform https://en.wikipedia.org/wiki/Identity_transform#Using_XSLT -->
	<xsl:template match="node()|@*">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
