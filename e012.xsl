<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e012-input.xml?>
<xsl:stylesheet version="2.0"
	xmlns:jh="http://www.jani-hur.net/xslt-functions"
	xmlns:output="http:www.output.com"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="jh xsl"
>
	<xsl:output
		encoding="utf-8"
		indent="yes"
		method="xml"
		omit-xml-declaration="yes"
	/>
	
	<!-- this makes whitespace-only input element b "empty" -->
	<xsl:strip-space elements="*"/>

	<xsl:template match="text()"/>

	<!-- TODO: a parameter what to do in case of 1) "empty" and 2) whitespace-only element -->
	
	<xsl:template match="input">
		<output:output>
			<output:template>
				<xsl:call-template name="optional">
					<xsl:with-param name="elem" select="a"/>
					<xsl:with-param name="name" select="'output:ta'"/>
				</xsl:call-template>
				<xsl:call-template name="optional">
					<xsl:with-param name="elem" select="b"/>
					<xsl:with-param name="name" select="'output:tb'"/>
				</xsl:call-template>
				<xsl:call-template name="optional">
					<xsl:with-param name="elem" select="c"/>
					<xsl:with-param name="name" select="'output:tc'"/>
				</xsl:call-template>
				<xsl:call-template name="optional">
					<xsl:with-param name="elem" select="d"/>
					<xsl:with-param name="name" select="'output:td'"/>
				</xsl:call-template>
			</output:template>

			<output:function>
				<xsl:copy-of select="jh:optional(a, 'output:fa')"/>
				<xsl:copy-of select="jh:optional(b, 'output:fb')"/>
				<xsl:copy-of select="jh:optional(c, 'output:fc')"/>
				<xsl:copy-of select="jh:optional(d, 'output:fd')"/>
			</output:function>
			
			<output:withmap>
				<xsl:variable name="elemMap">
					<map>
						<from>a</from>
						<to>output:ma</to>
					</map>
					<map>
						<from>b</from>
						<to>output:mb</to>
					</map>
					<map>
						<from>c</from>
						<to>output:mc</to>
					</map>
					<map>
						<from>d</from>
						<to>output:md</to>
					</map>
				</xsl:variable>

				<xsl:for-each select="*">
					<xsl:copy-of select="jh:mapElem(., $elemMap)"/>
				</xsl:for-each>
			</output:withmap>
		</output:output>
	</xsl:template>
	
	<xsl:template name="optional">
		<xsl:param name="elem"/>
		<xsl:param name="name" select="local-name($elem)"/>
		
		<xsl:if test="$elem">
			<xsl:element name="{$name}">
				<xsl:value-of select="$elem"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:function name="jh:optional">
		<xsl:param name="elem"/>
		<xsl:param name="newName"/>
		
		<xsl:if test="$elem">
			<xsl:element name="{$newName}">
				<xsl:value-of select="$elem"/>
			</xsl:element>
		</xsl:if>
	</xsl:function>

	<xsl:function name="jh:mapElem">
		<xsl:param name="elem"/>
		<xsl:param name="elemMap"/>
		
		<xsl:variable name="fromElemName" select="local-name($elem)"/>
		<xsl:variable name="toElemName" select="$elemMap/map[from = $fromElemName]/to/text()"/>

		<xsl:if test="$toElemName">
			<xsl:copy-of select="jh:optional($elem, $toElemName)"/>
		</xsl:if>
	</xsl:function>

</xsl:stylesheet>
