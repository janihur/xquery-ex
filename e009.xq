xquery version "1.0";

(: match a list of $values to a list of $regexps :)

(: a simple home made splitter (aka tokenize) :)
declare function local:split($input as xs:string?, $pattern as xs:string) as xs:string* {
	(: TODO requires trailing $pattern :)
	if ($input) then
		let $head as xs:string := substring-before($input, $pattern)
		let $tail as xs:string := substring-after($input, $pattern)
		return insert-before((), 2, ($head, local:split($tail, $pattern)))
	else
		()
};

(: use tokenize :)
declare function local:f1($values as xs:string, $regexps as xs:string) as element()* {
	for $value in tokenize($values, ',')
		for $regexp in tokenize($regexps, ',')
		let $match as xs:boolean := matches($value, $regexp)
		return
			<item>
				<value>{$value}</value>
				<regexp>{$regexp}</regexp>
				<match>{$match}</match>
			</item>
};

(: use a home-made tokenizer :)
declare function local:f2($values as xs:string, $regexps as xs:string) as element()* {
	for $value in local:split($values, ',')
		for $regexp in local:split($regexps, ',')
		let $match as xs:boolean := matches($value, $regexp)
		return
			<item>
				<value>{$value}</value>
				<regexp>{$regexp}</regexp>
				<match>{$match}</match>
			</item>
};

 local:f1('123,125,127', '124,125,1\d{2}$,1\d{3}$')
,local:f1('A,1,b', '\w,\d,\p{Lu}')
,local:f1('+358999814000', '\+358999814\d{3}')

(:
 local:f2('123,125,127,', '124,125,1\d{2}$,1\d{3}$,')
,local:f2('A,1,b,', '\w,\d,\p{Lu},')
,local:f2('+358999814000,', '\+358999814\d{3},')
:)

(:
 local:split('foo,bar,zoo,', ',')
,local:split('()', ',')
:)