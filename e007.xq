xquery version "1.0";

(: find a value from a sequence of 'key: value' strings :)

declare function local:xmlify($values as xs:string*) as element()? {
	if (not(empty($values)))
	then
		<kvlist>
		{
			let $separator as xs:string :=  ':'
			for $kv in $values
			return
				<kv>
					(: TODO trim :)
					  <key>{substring-before($kv, $separator)}</key>
					<value>{substring-after( $kv, $separator)}</value>
				</kv>
		}
		</kvlist>
	else
		()
};

declare function local:f($values as xs:string*) as xs:string {
	let $values2 as element()? := local:xmlify($values)
	let $k as xs:string := 'bar'
	let $v as xs:string := string($values2/kv[key = $k]/value)
	let $r as xs:string :=
		if ($v)
		then
			$v
		else
			'DEFAULT'
	
	return $r
};

local:f(
	('foo: 3', 'bar: 3')
),
local:f(
	('foo: 2')
),
local:f(
	'foo: 1'
),
local:f(
	('bar: 1')
),
local:f(
	('foo: ')
),
local:f(
	('bar: ')
),
local:f(
	('') (: this is not an empty sequence ! :)
),
local:f(
	() (: this is an empty sequence ! :)
)
