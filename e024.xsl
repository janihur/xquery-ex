<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e024-collection.xml?>
<!--
Combine (merge) two XML documents so that the value in the second document overwrites the value in the first document.
Intersection and minus set operations, XSLT 1 solution.
-->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	exclude-result-prefixes="xs fn"
>
	<xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:template match="collection">
		<xsl:variable name="firstSet" select="document(doc[1]/@href)"/>
		<xsl:variable name="secondSet" select="document(doc[2]/@href)"/>
		
		<xsl:variable name="firstIdSet" select="$firstSet/input/item/id"/>
		<xsl:variable name="secondIdSet" select="$secondSet/input/item/id"/>

		<xsl:variable name="firstOnlyIdSet" select="$firstIdSet[not(. = $secondIdSet)]"/>
		<xsl:variable name="secondOnlyIdSet" select="$secondIdSet[not(. = $firstIdSet)]"/>
		<xsl:variable name="bothIdSet" select="$firstIdSet[. = $secondIdSet]"/>
		
		<output>
			<filter description="one value (3) removed">
				<xsl:copy-of select="$secondIdSet[not(. = '3')]"/>
			</filter>
			
			<minus description="secondIdSet - firstIdSet">
				<xsl:copy-of select="$secondIdSet[not(. = $firstIdSet)]"/>
			</minus>
			
			<minus description="firstdIdSet - secondIdSet">
				<xsl:copy-of select="$firstIdSet[not(. = $secondIdSet)]"/>
			</minus>
			
			<intersection>
				<xsl:copy-of select="$firstIdSet[. = $secondIdSet]"/>
			</intersection>
			
			<merged description="third implementation">
				<xsl:apply-templates select="$firstOnlyIdSet">
					<xsl:with-param name="from" select="$firstSet"/>
				</xsl:apply-templates>
				<xsl:apply-templates select="$bothIdSet|$secondOnlyIdSet">
					<xsl:with-param name="from" select="$secondSet"/>
				</xsl:apply-templates>
			</merged>
			
			<merged description="second implementation">
				<!-- use first set if only available in the first set -->
				<xsl:for-each select="$firstOnlyIdSet">
					<xsl:variable name="id1" select="."/>
					<xsl:copy-of select="$firstSet/input/item[id = $id1]"/>
				</xsl:for-each>
				<!-- otherwise use the second set -->
				<xsl:for-each select="$bothIdSet">
					<xsl:variable name="id1" select="."/>
					<xsl:copy-of select="$secondSet/input/item[id = $id1]"/>
				</xsl:for-each>
				<xsl:for-each select="$secondOnlyIdSet">
					<xsl:variable name="id1" select="."/>
					<xsl:copy-of select="$secondSet/input/item[id = $id1]"/>
				</xsl:for-each>
			</merged>
			
			<merged description="first implementation">
				<xsl:variable name="secondOnlySet" select="$secondIdSet[not(. = $firstIdSet)]"/>
				
				<xsl:for-each select="$firstIdSet">
					<xsl:variable name="id1" select="."/>
					<xsl:variable name="secondHas" select="$secondIdSet[. = $id1]"/>
					<xsl:choose>
						<xsl:when test="$secondHas">
							<xsl:copy-of select="$secondSet/input/item[id = $id1]"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:copy-of select="$firstSet/input/item[id = $id1]"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				
				<xsl:for-each select="$secondOnlySet">
					<xsl:variable name="id1" select="."/>
					<xsl:copy-of select="$secondSet/input/item[id = $id1]"/>
				</xsl:for-each>
			</merged>

			<!--<TODO>
				<xsl:for-each select="$firstIdSet">
					<xsl:variable name="x" select="."/>
					<firstValue><xsl:value-of select="$x"/></firstValue>
					<isInSecondSet><xsl:value-of select="fn:boolean($secondIdSet[text() = $x])"/></isInSecondSet>
				</xsl:for-each>
			</TODO>-->
		</output>
	</xsl:template>
	
	<xsl:template match="id">
		<xsl:param name="from"/>
		<xsl:variable name="id" select="."/>
		<xsl:copy-of select="$from/input/item[id = $id]"/>
	</xsl:template>
	
</xsl:stylesheet>
