xquery version "1.0";

(: blacklist and whitelist filtering :)

declare function local:match($dataList as element(), $patternList as element()) as element() {
	<list>{
		for $data in data($dataList/*)
			for $pattern in data($patternList/*)
			let $match as xs:boolean := matches($data, $pattern)
			return <a match="{$match}">{$data}</a>
	}</list>
};

declare function local:unique($list as element()) as element() {
	<list>{
		for $a in $list/a[not(.=preceding::a)]
		return <a>{data($a)}</a>
	}</list>
};

declare function local:white($dataList as element(), $patternList as element()) as element() {
	let $all := local:match($dataList, $patternList)
	let $passed := <list>{
		$all/a[@match='true']
	}</list>
	return local:unique($passed)
};

declare function local:black($dataList as element(), $patternList as element()) as element() {
	let $all := local:match($dataList, $patternList)
	let $passed := <list>{
		let $distinct := distinct-values($all/a)
		for $a in $distinct
		let $blacklisted := boolean($all/a[.=$a and @match='true'])
		return
			if (not($blacklisted))
			then $all/a[.=$a]
			else ()
	}</list>
	return local:unique($passed)
};

(: element names in the input variables doesn't matter but the structure does :)

declare variable $input :=
	<input>{
		for $i in ((0 to 9), 2, 4) return <elem>{$i}</elem>
	}</input>
;

declare variable $whitelist :=
	<whitelist>
		<a>0</a>
		<b>2</b>
		<c>4</c>
		<d>6</d>
		<e>8</e>
		<f>10</f>
	</whitelist>
;

declare variable $blacklist :=
	<blacklist>
		<pattern>1</pattern>
		<pattern>3</pattern>
		<pattern>5</pattern>
		<pattern>7</pattern>
		<pattern>9</pattern>
		<pattern>11</pattern>
	</blacklist>
;

(:
local:match($input, $whitelist)
:)

 local:white($input, $whitelist)
,local:black($input, $blacklist)
,<validValues>{
   for $a in local:white($input, $whitelist)/*
   return <value>{data($a)}</value>
 }</validValues>
