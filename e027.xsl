<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e026-input-1.xml?>
<!--
XSLT30: convert json to xml representation of json and then process that xml further to actual wanted xml
-->
<xsl:stylesheet version="3.0"
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	exclude-result-prefixes="fn"
>
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"/>

	<!-- the json payload is wrapped inside xml element. the name of the element is irrelevant -->
	<xsl:template match="/json">
		<!-- convert json to xml representation of json and apply templates to that xml -->
		<xsl:apply-templates select="fn:json-to-xml(.)"/>
	</xsl:template>

	<!-- templates to transform xml representation of json to actual wanted xml -->
	<xsl:template match="/fn:map">
		<distances>
			<xsl:apply-templates select="fn:map[@key = 'cities']/fn:array"/>
		</distances>
	</xsl:template>

	<xsl:template match="fn:array">
		<xsl:for-each select="fn:map">
			<distance>
				<from><xsl:value-of select="../@key"/></from>
				<to><xsl:value-of select="fn:string[@key = 'to']"/></to>
				<value><xsl:value-of select="fn:number[@key = 'distance']"/></value>
			</distance>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
