xquery version "1.0";

(: blacklist and whitelist filtering variation 2 :)

declare function local:match($dataList as element(), $patternList as element()) as element() {
	<list>{
		for $data in data($dataList/*)
			for $pattern in data($patternList/*)
			let $match as xs:boolean := matches($data, $pattern)
			return <a match="{$match}">{$data}</a>
	}</list>
};

declare function local:impl($dataList as element(), $patternList as element(), $status as xs:string*) as element() {
	let $all := local:match($dataList, $patternList)
	let $distinct := distinct-values($all/a)
	return <list>{
		for $a in $distinct
		let $match as xs:boolean := boolean($all/a[.=$a and @match='true'])
		let $status := if ($match) then $status[1] else $status[2]
		return <a status="{$status}">{$a}</a>
	}</list>
};

declare function local:white($dataList as element(), $patternList as element()) as element() {
	local:impl($dataList, $patternList, ('allowed', 'blocked'))
};

declare function local:black($dataList as element(), $patternList as element()) as element() {
	local:impl($dataList, $patternList, ('blocked', 'allowed'))
};

(: element names in the input variables doesn't matter but the structure does :)

declare variable $input :=
	<input>{
		for $i in ((0 to 9), 2, 4) return <elem>{$i}</elem>
	}</input>
;

declare variable $whitelist :=
	<whitelist>
		<a>0</a>
		<b>2</b>
		<c>4</c>
		<d>6</d>
		<e>8</e>
		<f>10</f>
	</whitelist>
;

declare variable $blacklist :=
	<blacklist>
		<pattern>1</pattern>
		<pattern>3</pattern>
		<pattern>5</pattern>
		<pattern>7</pattern>
		<pattern>9</pattern>
		<pattern>11</pattern>
	</blacklist>
;

 local:white($input, $whitelist)
,local:black($input, $blacklist)
,<allowedValues>{
   for $a in local:white($input, $whitelist)/*[@status='allowed']
   return <value>{data($a)}</value>
 }</allowedValues>
 ,<blockedValues>{
   for $a in local:white($input, $whitelist)/*[@status='blocked']
   return <value>{data($a)}</value>
 }</blockedValues>