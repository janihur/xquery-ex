<?xml version="1.0" encoding="UTF-8"?>
<?altova_samplexml e033-input.xml?>
<!--
change element namespace
-->
<xsl:stylesheet version="2.0" 
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:from="from-namespace"
	xmlns:b="to-namespace"
>
	<xsl:output encoding="utf-8" indent="yes" method="xml"/>

	<!-- change element namespace -->
	<xsl:template match="from:*">
		<xsl:element name="b:{fn:local-name()}">
			<xsl:apply-templates select="node()|@*"/>
		</xsl:element>
	</xsl:template>
	
	<!-- copy other nodes -->
	<xsl:template match="@*|text()|comment()|processing-instruction()">
		<xsl:copy/>
	</xsl:template>

</xsl:stylesheet>
